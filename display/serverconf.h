#pragma once

#include <QWidget>
#include <QStringList>
#include <memory>

class Scoreboard;
class QTextEdit;
class Settings;

class ServerConf : public QWidget
{
public:
    ServerConf(Settings& settings);
    void appendMsg(QString msg);

private:
    void enableServer(bool on);
    std::unique_ptr<Scoreboard> server;
    QStringList log;
    QTextEdit *logW;
    Settings& settings;
};

