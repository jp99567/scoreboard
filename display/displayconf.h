#pragma once

#include <memory>
#include <QWidget>
#include <QTcpSocket>
#include <QTimer>
#include <vector>
#include "scoreboard.h"
#include "LineParser.h"

class QLabel;
class QCheckBox;
class QLineEdit;
class QComboBox;
class QPushButton;
class QSpinBox;
class QRadioButton;
class QTextToSpeech;
class QMediaPlayer;
class Settings;

namespace scoreboard {
class Display;
}

class DisplayConf : public QWidget
{
    Q_OBJECT
public:
    explicit DisplayConf(Settings& settings, QWidget *parent = nullptr);
    ~DisplayConf();
    void setDisplay(scoreboard::Display* disp)
    {
        display = disp;
    }

    scoreboard::Protocol::Event volumeUpEvent() const;

signals:
    void showDisplay();

public slots:
    void onPlusA();
    void onPlusB();
    void scoreBack();

private slots:
    void scheduleReconnect(int ms);
private:
    QLabel* connStatus;
    QCheckBox* enabled;
    QLineEdit* host;
    QComboBox* role;
    QCheckBox* toSpeech;
    QCheckBox* soundPlayer;
    QPushButton *plusA, *plusB;
    QSpinBox *scoreL, *scoreR, *setL, *setR;
    QRadioButton *servisNon, *servisL, *servisR;
    QComboBox* volUpEvent;
    scoreboard::Protocol::DisplayFeatures features;

    QTcpSocket socket;
    QTimer reconnect;
    LineParser in;
    scoreboard::Display *display;

    QMetaObject::Connection speechStateConn;
    QMetaObject::Connection sounPlayerStateConn;
    bool speechToSayReq = false;
    QString soundToPlay;

#ifdef ZVUKY_SUPPORT
    std::unique_ptr<QMediaPlayer> sound;
#ifdef SCORE2SPECH_SUPPORT
    std::unique_ptr<QTextToSpeech> speech;
#endif
#endif

    void initSocket();

    void enableSpeech(bool on);
    void sendLine(QString line);
    void sendRole();
    void sendFeatures();
    void sendEvent(scoreboard::Protocol::Event e);
    void sendScoreCorrection();
    void processLine(std::string line);
    void updateScore(scoreboard::Score& score);
    void sayScore(const scoreboard::Score& score);
    void playSound(QString file);
    void enableMediaPlayer(bool enable);
    QUrl searchAudioFile(QString sound);
    static QStringList initAudioSerchPath();

    Settings& settings;
    QStringList audioSerchDirs;
    std::vector<QMetaObject::Connection> qobjDisconnectOnExit;
    scoreboard::Score currentScore;
};
