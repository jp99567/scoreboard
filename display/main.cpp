#include <QApplication>
#include <QTabWidget>
#include <QStackedWidget>
#include <QMainWindow>
#include <QCloseEvent>
#include <QTextEdit>
#include <sstream>

#ifdef ANDROID
#include <QJniObject>
#endif

#include "git_revision_description.h"
#include "displayconf.h"
#include "display.h"
#include "serverconf.h"
#include "settings.h"
#ifndef WINNT
#include "flicstupidhttpd.h"
#endif

static std::string versionTxt()
{
    std::ostringstream ss;
    ss << "DateTime: " << __DATE__ << ' ' << __TIME__ << std::endl
#ifdef Git_FOUND
       << "Version: " << GIT_REV_DESC_STR << std::endl
#endif
       << "Toolchain: " << __VERSION__ << std::endl
       << "QT: " << QT_VERSION_STR << std::endl;

   return ss.str();
}

class MainWindow : public QMainWindow
{
    Settings settings;
    DisplayConf *dispConf;

#ifndef WINNT
    FlicStupidHttpd flicHttpd;
#endif

public:
    explicit MainWindow()
#ifndef WINNT
        :flicHttpd(this)
#endif
    {
#ifdef ANDROID
        setStyleSheet("QCheckBox::indicator { width:20px; height: 20px;} QRadioButton::indicator { width:20px; height: 20px;}");
#endif
        auto mainTab = new QTabWidget;
        dispConf = new DisplayConf(settings);
        auto display = new scoreboard::Display;
        dispConf->setDisplay(display);
        mainTab->addTab(dispConf, "Display Config");
        auto serverConf = new ServerConf(settings);
        mainTab->addTab(serverConf, "Server Config");
        auto version = new QTextEdit;
        version->setText(QString::fromStdString(versionTxt()));;
        version->setReadOnly(true);
        mainTab->addTab(version, "SW Info");
        auto cw = new QStackedWidget;
        cw->addWidget(mainTab);
        cw->addWidget(display);
        setCentralWidget(cw);

        connect(dispConf, &DisplayConf::showDisplay, [this]{
            static_cast<QStackedWidget*>(centralWidget())->setCurrentIndex(1);
        });

#ifndef WINNT
        connect(&flicHttpd, &FlicStupidHttpd::plusA, dispConf, &DisplayConf::onPlusA);
        connect(&flicHttpd, &FlicStupidHttpd::plusB, dispConf, &DisplayConf::onPlusB);
        connect(&flicHttpd, &FlicStupidHttpd::batteryLow, [serverConf](int level){
            serverConf->appendMsg(QString("button battery level: %1").arg(level));
        });
#endif
    }

    void closeEvent(QCloseEvent *event) override
    {
        auto cw = static_cast<QStackedWidget*>(centralWidget());
        qDebug() << "closeEvent" << event << cw->currentIndex();
        if( cw->currentIndex() > 0 ){
            cw->setCurrentIndex(0);
            event->ignore();
        }
    }

    void keyPressEvent(QKeyEvent *event) override
    {
        if(event->key() == Qt::Key_VolumeUp && dispConf->volumeUpEvent() != scoreboard::Protocol::Event::Non)
               return;
        QMainWindow::keyPressEvent(event);
    }

    void keyReleaseEvent(QKeyEvent *event) override
    {
        qDebug() << "release" << event;
        if(event->key() == Qt::Key_Return){
            dispConf->scoreBack();
        }
        else if(event->key() == Qt::Key_VolumeUp){
            auto etype = dispConf->volumeUpEvent();
            if(etype != scoreboard::Protocol::Event::Non){
                if(dispConf->volumeUpEvent() != scoreboard::Protocol::Event::PlusA)
                    dispConf->onPlusA();
                else if(dispConf->volumeUpEvent() != scoreboard::Protocol::Event::PlusB)
                    dispConf->onPlusB();
                return;
            }
        }
        else if(event->key() == Qt::Key_F8){
            dispConf->onPlusA();
        }
        else if(event->key() == Qt::Key_F9){
            dispConf->onPlusB();
        }

        QMainWindow::keyPressEvent(event);
    }
};

struct ScopedExit
{
    std::function<void()> f;

    ScopedExit(std::function<void()> f) : f(f){}

    ~ScopedExit()
    {
        f();
    }
};

int main(int argc, char *argv[])
{
    ScopedExit fine([]{
        qDebug() << "GRACEFUL";
    });

    qputenv("QT_ANDROID_VOLUME_KEYS", "1");
    QApplication a(argc, argv);    

    MainWindow w;

#ifdef ANDROID
    QJniObject activity = QNativeInterface::QAndroidApplication::context();
    if(activity.isValid()){
        auto window = activity.callObjectMethod("getWindow", "()Landroid/view/Window;");
        if (window.isValid()) {
            const int FLAG_KEEP_SCREEN_ON = 128;
            window.callMethod<void>("addFlags", "(I)V", FLAG_KEEP_SCREEN_ON);
        }
    }

    //Clear any possible pending exceptions.
    QJniEnvironment env;
    if(env->ExceptionCheck()){
        env->ExceptionClear();
    }
#endif

    w.show();
    return a.exec();
}
