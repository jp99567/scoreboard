#include "displayconf.h"

#include <QFormLayout>
#include <QLabel>
#include <QCheckBox>
#include <QLineEdit>
#include <QComboBox>
#include <QPushButton>
#include <QSpinBox>
#include <QRadioButton>
#include <QFrame>
#include <QTimer>
#include <QMetaEnum>
#include <QHostAddress>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>

#ifdef ZVUKY_SUPPORT
#ifdef SCORE2SPECH_SUPPORT
#include <QTextToSpeech>
#endif
#include <QMediaPlayer>
#include <QAudioOutput>
#endif

#include <QFileInfo>
#include "scoreboard.h"
#include <sstream>
#include "display.h"
#include "settings.h"

static void setNoFocus(QWidget* w)
{
     w->setFocusPolicy(Qt::FocusPolicy::NoFocus);
}

DisplayConf::DisplayConf(Settings& settings, QWidget *parent)
    :QWidget(parent)
    ,settings(settings)
    ,audioSerchDirs(initAudioSerchPath())
{
    auto vl = new QVBoxLayout;
    auto fl = new QFormLayout;
    connStatus = new QLabel;
    fl->addRow(connStatus);
    enabled = new QCheckBox;
    enabled->setChecked(settings.connectionEnabled());
    setNoFocus(enabled);
    host = new QLineEdit;
    host->setText(settings.host());
    fl->addRow("Server", host);

    role = new QComboBox;
    setNoFocus(role);
    {
        using scoreboard::Protocol;
        auto roleTxt = QMetaEnum::fromType<Protocol::DisplayRole>();
        role->addItem(roleTxt.valueToKey((int)Protocol::DisplayRole::non));
        role->addItem(roleTxt.valueToKey((int)Protocol::DisplayRole::L1));
        role->addItem(roleTxt.valueToKey((int)Protocol::DisplayRole::L2));
        role->addItem(roleTxt.valueToKey((int)Protocol::DisplayRole::R1));
        role->addItem(roleTxt.valueToKey((int)Protocol::DisplayRole::R2));
        role->setCurrentIndex((int)settings.displayRole());
    }
    fl->addRow("Display Role", role);

    connect(role, &QComboBox::currentTextChanged, [this]{
        sendRole();
        this->settings.displayRole((scoreboard::Protocol::DisplayRole)role->currentIndex());
    });

    toSpeech = new QCheckBox;
    toSpeech->setChecked(settings.scoreSpeech());
    toSpeech->setFocusPolicy(Qt::FocusPolicy::NoFocus);
    fl->addRow("Say Score", toSpeech);
    connect(toSpeech, &QCheckBox::clicked, this, &DisplayConf::enableSpeech);
    enableSpeech(toSpeech->isChecked());
    features = settings.features();
    soundPlayer = new QCheckBox;
    soundPlayer->setChecked(features.testFlag(scoreboard::Protocol::DisplayFeature::PlaySound));
    soundPlayer->setFocusPolicy(Qt::FocusPolicy::NoFocus);
    fl->addRow("Play Sounds", soundPlayer);
    connect(soundPlayer, &QCheckBox::clicked, [this](bool checked){
        features.setFlag(scoreboard::Protocol::DisplayFeature::PlaySound, checked);
        sendFeatures();
        this->settings.features(static_cast<scoreboard::Protocol::DisplayFeature>((int)features));
        enableMediaPlayer(checked);
    });
    enableMediaPlayer(soundPlayer->isChecked());

    volUpEvent = new QComboBox;
    setNoFocus(volUpEvent);
    {
        using scoreboard::Protocol;
        auto actionTxt = QMetaEnum::fromType<Protocol::Event>();
        volUpEvent->addItem(actionTxt.valueToKey((int)Protocol::Event::PlusA));
        volUpEvent->addItem(actionTxt.valueToKey((int)Protocol::Event::PlusB));
        volUpEvent->addItem(actionTxt.valueToKey((int)Protocol::Event::Non));
        volUpEvent->setCurrentIndex((int)settings.volUpKyeAction());
    }
    fl->addRow("Volume UP action", volUpEvent);

    connect(volUpEvent, &QComboBox::currentTextChanged, [this]{
        this->settings.volUpKyeAction((scoreboard::Protocol::Event)volUpEvent->currentIndex());
    });


    auto connect_label = new QLabel("connect");
    connect_label->setStyleSheet("font-weight: bold");
    fl->addRow(connect_label, enabled);

    vl->addLayout(fl);

    auto newHLine = []{
        auto hLine = new QFrame;
        hLine->setFrameShadow(QFrame::Shadow::Sunken);
        hLine->setFrameShape(QFrame::Shape::HLine);
        return hLine;
    };
    vl->addWidget(newHLine());

    plusA = new QPushButton("Plus &A");
    plusB = new QPushButton("Plus &B");

    vl->addWidget(plusA);
    vl->addWidget(plusB);
    vl->addWidget(newHLine());

    QPushButton *apply, *back;
    {
        auto scoreCorrectionFrame = new QGroupBox;
        scoreCorrectionFrame->setTitle("Score Corrections");
        auto gl = new QGridLayout;
        int row = 0;
        back = new QPushButton("Back");
        back->connect(back, &QPushButton::clicked, this, &DisplayConf::scoreBack);
        gl->addWidget(back, row++, 0, 1, 3);
        scoreL = new QSpinBox;
        scoreR = new QSpinBox;
        setL = new QSpinBox;
        setR = new QSpinBox;
        gl->addWidget(new QLabel("Score"), row, 0);
        gl->addWidget(scoreL, row, 1);
        gl->addWidget(scoreR, row++, 2);
        gl->addWidget(new QLabel("Set"), row, 0);
        gl->addWidget(setL, row, 1);
        gl->addWidget(setR, row++, 2);
        servisNon = new QRadioButton("ServisNon");
        servisL = new QRadioButton("ServisL");
        servisR = new QRadioButton("ServisR");
        for( QWidget *w : {servisL, servisR, servisNon})
            w->setFocusPolicy(Qt::FocusPolicy::NoFocus);
        gl->addWidget(servisNon, row, 0);
        gl->addWidget(servisL, row, 1);
        gl->addWidget(servisR, row++, 2);
        apply = new QPushButton("Apply Corrections");
        gl->addWidget(apply, row++, 0, 1, 3);
        scoreCorrectionFrame->setLayout(gl);
        vl->addWidget(scoreCorrectionFrame);
    }

    for( QWidget *w : {plusA, plusB, apply, back})
        w->setFocusPolicy(Qt::FocusPolicy::NoFocus);

    setLayout(vl);

    connect(plusA, &QPushButton::clicked, this, &DisplayConf::onPlusA);
    connect(plusB, &QPushButton::clicked, this, &DisplayConf::onPlusB);
    connect(apply, &QPushButton::clicked, this, &DisplayConf::sendScoreCorrection);

    initSocket();
    if(enabled->isChecked())
        scheduleReconnect(100);
}

DisplayConf::~DisplayConf()
{
    enableMediaPlayer(false);
    for(auto qmc : qobjDisconnectOnExit)
        disconnect(qmc);

    qDebug() << "~DisplayConf";
}

scoreboard::Protocol::Event DisplayConf::volumeUpEvent() const
{
    return (scoreboard::Protocol::Event)volUpEvent->currentIndex();
}

void DisplayConf::onPlusA()
{
    sendEvent(scoreboard::Protocol::Event::PlusA);
}

void DisplayConf::onPlusB()
{
    sendEvent(scoreboard::Protocol::Event::PlusB);
}

void DisplayConf::scoreBack()
{
    sendLine(QMetaEnum::fromType<scoreboard::Protocol::ServerRequest>()
             .valueToKey((int)scoreboard::Protocol::ServerRequest::ScoreBack));
}


void DisplayConf::scheduleReconnect(int ms)
{
    qDebug() << "schedule reconnect";
    reconnect.start(ms);
}

void DisplayConf::initSocket()
{
    connect(enabled, &QCheckBox::clicked, this, [this](bool checked){
        qDebug() << "conn enabled" << checked;
        settings.connectionEnabled(checked);
        settings.host(host->text());
        if(checked)
        {
            if(socket.state() == QAbstractSocket::SocketState::UnconnectedState)
            {
                socket.connectToHost(host->text(), scoreboard::port);
            }
        }
        else
        {
            reconnect.stop();
            if(   socket.state() == QAbstractSocket::SocketState::ConnectedState
               || socket.state() == QAbstractSocket::SocketState::ConnectingState
               || socket.state() == QAbstractSocket::SocketState::HostLookupState )
            {
                socket.disconnectFromHost();
            }
        }
    });

    auto qmc = connect(&socket, &QTcpSocket::stateChanged, [this](QAbstractSocket::SocketState state){
        auto txt = QMetaEnum::fromType<QAbstractSocket::SocketState>();

        qDebug() << txt.valueToKey(state);

        switch(state)
        {
          case QAbstractSocket::SocketState::UnconnectedState:
          {
            if(enabled->isChecked())
            {
                scheduleReconnect(5000);
            }
            connStatus->setText(txt.valueToKey(state));
          }
            break;
          case QAbstractSocket::SocketState::ConnectedState:
          {
            settings.host(host->text());
            settings.sync();
            auto name = socket.peerName();
            auto addr = socket.peerAddress().toString();
            if(addr == name){
                connStatus->setText( QString("Connected to %1")
                                 .arg(name));
            }
            else{
                connStatus->setText( QString("Connected to %1(%2)")
                                 .arg(socket.peerName())
                                 .arg(socket.peerAddress().toString()));
            }
            sendRole();
            sendFeatures();
          }
            break;
          default:
            connStatus->setText(txt.valueToKey(state));
            break;
        }
    });
    qobjDisconnectOnExit.push_back(qmc);

    qmc = connect(&socket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::errorOccurred),
          [this](QAbstractSocket::SocketError socketError){
        qDebug() << "connection error" << socketError;
        connStatus->setText(socket.errorString());
        if(enabled->isChecked())
        {
            display->setMainBgColor("blue");
            display->setAuxBgColor("blue");
            display->setMainTxtColor("black");
            display->setAuxText("");
            display->setMainChar('X');
        }
    });
    qobjDisconnectOnExit.push_back(qmc);

    qmc = connect(&socket, &QTcpSocket::readyRead, [this]{in.process(socket.readAll().toStdString());});
    connect(&in, &LineParser::newLine, this, &DisplayConf::processLine);
    qobjDisconnectOnExit.push_back(qmc);

    reconnect.setSingleShot(true);
    qmc = connect(&reconnect, &QTimer::timeout, [this]{
        if(enabled->isChecked())
        {
            qDebug() << "reconnect";
            socket.connectToHost(host->text(), scoreboard::port);
        }
    });
    qobjDisconnectOnExit.push_back(qmc);
}

QString createScoreText(const scoreboard::Score& score)
{
    int v1=0, v2=0;

    if(score.servis == scoreboard::Protocol::Servis::A){
        std::tie(v1, v2) = std::tie( score.a, score.b);
    }
    else if(score.servis == scoreboard::Protocol::Servis::B){
        std::tie(v1, v2) = std::tie( score.b, score.a);
    }


    if(Scoreboard::isMatchball(v1, v2))
        return "matchball";

    return QString("%1. %2.")
        .arg(v1)
        .arg(v2);
}

void DisplayConf::enableSpeech(bool on)
{
#ifdef SCORE2SPECH_SUPPORT
    settings.scoreSpeech(on);
    if(on)
    {
        if(speech)
            return;

        speech = std::make_unique<QTextToSpeech>();
        speechStateConn = connect(speech.get(), &QTextToSpeech::stateChanged, [this](QTextToSpeech::State state)
        {
            qDebug() << state;
            switch(state)
            {
                case QTextToSpeech::State::Ready:
                {
                    if(speechToSayReq)
                    {
                        speechToSayReq = false;
                        speech->say(createScoreText(currentScore));
                    }
                    else if(not soundToPlay.isEmpty())
                    {
                        playSound(soundToPlay);
                    }
                    break;
                }
                default:
                    break;
            }
        });
    }
    else
    {
        if(speech)
        {
            disconnect(speechStateConn);
        }
        speech = nullptr;
        speechToSayReq = false;
    }
#endif
}

void DisplayConf::sendLine(QString line)
{
    auto data = line + '\n';
    auto len = socket.write(data.toUtf8());
    //qDebug() << "written" << len << line;
    Q_UNUSED(len)
}

void DisplayConf::sendRole()
{
    auto reqTxt = QMetaEnum::fromType<scoreboard::Protocol::ServerRequest>();
    sendLine(
         QString(reqTxt.valueToKey((int)scoreboard::Protocol::ServerRequest::InfoDisplayRole))
         + ' ' + role->currentText());
}

void DisplayConf::sendFeatures()
{
    auto reqTxt = QMetaEnum::fromType<scoreboard::Protocol::ServerRequest>();
    auto featuresTxt = QMetaEnum::fromType<scoreboard::Protocol::DisplayFeature>();
    sendLine(
         QString(reqTxt.valueToKey((int)scoreboard::Protocol::ServerRequest::InfoFeatures))
         + ' ' + featuresTxt.valueToKeys(features));
}

void DisplayConf::sendEvent(scoreboard::Protocol::Event e)
{
    auto reqTxt = QMetaEnum::fromType<scoreboard::Protocol::ServerRequest>();
    auto eTxt = QMetaEnum::fromType<scoreboard::Protocol::Event>();
    sendLine(reqTxt.valueToKey((int)scoreboard::Protocol::ServerRequest::AcceptEvent)
             + QString(" ")
             + eTxt.valueToKey((int)e));
}

void DisplayConf::sendScoreCorrection()
{
    scoreboard::Score score;

    score.a = scoreL->value();
    score.b = scoreR->value();
    score.setA = setL->value();
    score.setB = setR->value();

    if(servisL->isChecked())
        score.servis = scoreboard::Protocol::Servis::A;
    else if(servisR->isChecked())
        score.servis = scoreboard::Protocol::Servis::B;
    else
        score.servis = scoreboard::Protocol::Servis::non;

    if( not score.teamAonLeft())
        score.swapSide();

    auto reqTxt = QMetaEnum::fromType<scoreboard::Protocol::ServerRequest>();
    QString req = QString(reqTxt.valueToKey((int)scoreboard::Protocol::ServerRequest::SetScore))
            + ' ' + Scoreboard::serializeScore(score);
    sendLine(std::move(req));
}

static QString readRestOfStream(std::istringstream& ss)
{
    auto pos = 1 + ss.tellg();
    if(pos < (long)ss.str().size())
    {
        return QByteArray::fromStdString(ss.str().substr(pos));
    }
    return "";
}

void DisplayConf::processLine(std::string line)
{
    //qDebug() << "processLine" << QString::fromStdString(line);
    auto indME = QMetaEnum::fromType<scoreboard::Protocol::ServerIndication>();
    bool ok = false;
    std::istringstream ss;
    ss.str(line);
    std::string arg;
    ss >> arg;
    auto ind = (scoreboard::Protocol::ServerIndication)indME.keyToValue(arg.c_str(), &ok);

    if(!ok)
    {
        socket.disconnectFromHost();
        return;
    }

    switch(ind) {
    case scoreboard::Protocol::ServerIndication::InfoScore:
    {
        scoreboard::Score score;
        Scoreboard::deserializeScore(ss, currentScore);
        updateScore(currentScore);
        sayScore(currentScore);
        break;
    }
    case scoreboard::Protocol::ServerIndication::PlaySound:
    {
        ss >> arg;
        playSound(QString::fromStdString(arg));
        break;
    }
    case scoreboard::Protocol::ServerIndication::SetMainDisplayBgColor:
    {
        ss >> arg;
        display->setMainBgColor(QString::fromStdString(arg));
        emit showDisplay();
        break;
    }
    case scoreboard::Protocol::ServerIndication::SetAuxDisplayBgColor:
    {
        ss >> arg;
        display->setAuxBgColor(QString::fromStdString(arg));
        emit showDisplay();
        break;
    }
    case scoreboard::Protocol::ServerIndication::SetMainDisplayTextColor:
    {
        ss >> arg;
        display->setMainTxtColor(QString::fromStdString(arg));
        emit showDisplay();
        break;
    }
    case scoreboard::Protocol::ServerIndication::SetAuxDisplayTextColor:
    {
        ss >> arg;
        display->setAuxTxtColor(QString::fromStdString(arg));
        emit showDisplay();
        break;
    }
    case scoreboard::Protocol::ServerIndication::SetMainDisplayText:
    {
        auto str = readRestOfStream(ss);
        if(not str.isEmpty()){
            display->setMainChar(str.front());
            emit showDisplay();
        }
        break;
    }
    case scoreboard::Protocol::ServerIndication::SetAuxDisplayText:
    {
        auto str = readRestOfStream(ss);
        display->setAuxText(str);
        emit showDisplay();
        break;
    }
    default:
        break;
    }
}

void DisplayConf::updateScore(scoreboard::Score &score)
{
    if( not score.teamAonLeft())
        score.swapSide();

    scoreL->setValue(score.a);
    scoreR->setValue(score.b);
    setL->setValue(score.setA);
    setR->setValue(score.setB);
    switch (score.servis) {
    case scoreboard::Protocol::Servis::A:
        servisL->setChecked(true);
        break;
    case scoreboard::Protocol::Servis::B:
        servisR->setChecked(true);
        break;
    default:
        servisNon->setChecked(true);
        break;
    }
}

void DisplayConf::sayScore(const scoreboard::Score& score)
{
#ifdef SCORE2SPECH_SUPPORT
    if(speech)
    {
            if(Scoreboard::isSetFinished(score.a, score.b)
                || (score.a==0 && score.b==0)){
                speechToSayReq = false;
                return;
            }

            if(!sound || ( sound->playbackState() == QMediaPlayer::PlaybackState::StoppedState
                      && sound->mediaStatus() != QMediaPlayer::MediaStatus::LoadingMedia
                      && sound->mediaStatus() != QMediaPlayer::MediaStatus::BufferingMedia))
            {
                if(speech->state() == QTextToSpeech::State::Ready)
                {
                    speech->say(createScoreText(score));
                    speechToSayReq = false;
                    return;
                }
                else{
                    speech->stop();
                }
            }
            speechToSayReq = true;
    }
#endif
}

void DisplayConf::playSound(QString file)
{
#ifdef ZVUKY_SUPPORT
    if(features.testFlag(scoreboard::Protocol::DisplayFeature::PlaySound))
    {
        auto url = searchAudioFile(file);
        qDebug() << "audio" << url << url.isEmpty() << url.isValid() << file;
        if(url.isValid())
        {
#ifdef SCORE2SPECH_SUPPORT
            bool este_hlasi = speech && speech->state() == QTextToSpeech::State::Speaking;
#else
            bool este_hlasi = false;
#endif
            if(este_hlasi)
            {
                soundToPlay = file;
                qDebug() << "sound postponed" << soundToPlay;
                return;
            }
            else if(sound)
            {
                if(sound->source() != url)
                    sound->setSource(url);
                qDebug() << "play0" << sound->playbackState() << sound->mediaStatus();
                sound->play();
                qDebug() << "play1" << sound->playbackState() << sound->mediaStatus() << sound->audioOutput()->volume() << sound->audioOutput()->isMuted();
            }
        }
    }
    soundToPlay = "";
#endif
}

void DisplayConf::enableMediaPlayer(bool enable)
{
#ifdef ZVUKY_SUPPORT
    if(enable)
    {
        if(sound)
            return;
        try {
            auto ao = new QAudioOutput(this);
            sound = std::make_unique<QMediaPlayer>(this);
            sound->setAudioOutput(ao);
            connect(sound.get(), &QMediaPlayer::mediaStatusChanged, [this](QMediaPlayer::MediaStatus s){
                qDebug() << "mediaStatusChanged" << s << sound->playbackState();
                if(s==QMediaPlayer::MediaStatus::LoadedMedia
                    ||s==QMediaPlayer::MediaStatus::BufferedMedia){
                    sound->play();
                    qDebug() << "play2" << sound->playbackState() << sound->mediaStatus();
                }
            });
        }
        catch(...){
            qWarning() << "exception during audio instantiation";
            sound = nullptr;
        }

        sounPlayerStateConn = connect(sound.get(), &QMediaPlayer::playbackStateChanged, [this](QMediaPlayer::PlaybackState state){
            qDebug() << "playbackStateChanged" << state << sound->mediaStatus();
            if(state == QMediaPlayer::PlaybackState::StoppedState)
            {
#ifdef SCORE2SPECH_SUPPORT
                if(speech && speechToSayReq)
                {
                    speech->say(createScoreText(currentScore));
                    speechToSayReq = false;
                }
#endif
            }
        });
    }
    else{
        if(sound)
        {
            disconnect(sounPlayerStateConn);
            sound = nullptr;
        }
    }
#endif
}

QUrl DisplayConf::searchAudioFile(QString sound)
{
    for(auto& dir : audioSerchDirs)
    {
        auto path = dir + '/' + sound;
        QFileInfo info(path);
        if(info.isFile() && info.isReadable())
        {
            return QUrl::fromLocalFile(info.absoluteFilePath());
        }
    }

    return QUrl(QString("qrc:/").append(sound));
}

QStringList DisplayConf::initAudioSerchPath()
{
    QStringList rv;

    rv << ".";

#ifndef ANDROID
    rv << "../scoreboard-sounds";
#endif

#ifdef ANDROID
    rv << "/sdcard/Download/scoreboard-sounds"
       << "/sdcard/Download";
#elif __linux__
    rv << "/usr/share/korganizer/sounds"
       << "/usr/lib/libreoffice/share/gallery/sounds";
#endif
    return rv;
}
