#pragma once

#include <QWidget>

class QLabel;
class QResizeEvent;

namespace scoreboard {

class Display : public QWidget
{
    Q_OBJECT

public:
    Display(QWidget *parent = nullptr);
    ~Display() override;

    void setMainBgColor(QString color);
    void setMainTxtColor(QString color);
    void setAuxBgColor(QString color);
    void setAuxTxtColor(QString color);
    void setMainChar(QChar c);
    void setAuxText(QString s);

private:
    QLabel* main;
    QLabel* aux;

protected:
    void resizeEvent(QResizeEvent *event) override;
};
}
