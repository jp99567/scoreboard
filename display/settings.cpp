#include "settings.h"

#include <QMetaEnum>

Settings::Settings()
#ifdef ANDROID
    :s("VolejbalRiazanska", "Scoreboard")
#else
    :s("scoreboard.config", QSettings::IniFormat)
#endif
{
}

namespace
{

constexpr auto ConnectionEnabled = "ConnectionEnabled";
constexpr auto Host = "Host";
constexpr auto DisplayRole = "DisplayRole";
constexpr auto ScoreSpeech = "ScoreSpeech";
constexpr auto DisplayFeatures = "DisplayFeatures";
constexpr auto ServerEnabled = "ServerEnabled";
constexpr auto VolumeUpAction = "VolumeUpAction";

}

bool Settings::connectionEnabled() const
{
    auto v = s.value(ConnectionEnabled);
    return v.toBool();
}

void Settings::connectionEnabled(bool on)
{
    QVariant v(on);
    s.setValue(ConnectionEnabled, v);
}

QString Settings::host() const
{
    auto v = s.value(Host, "localhost");
    return v.toString();
}

void Settings::host(QString host)
{
    QVariant v(host);
    s.setValue(Host, v);
}

scoreboard::Protocol::DisplayRole Settings::displayRole() const
{
    auto roleTxt = QMetaEnum::fromType<scoreboard::Protocol::DisplayRole>();
    auto v = s.value(DisplayRole);

    v.toString();
    bool ok=false;
    auto rv = (scoreboard::Protocol::DisplayRole)roleTxt.keyToValue(v.toString().toStdString().c_str(), &ok);

    if(not ok)
        return scoreboard::Protocol::DisplayRole::non;

    return rv;
}

void Settings::displayRole(scoreboard::Protocol::DisplayRole role)
{
    auto roleTxt = QMetaEnum::fromType<scoreboard::Protocol::DisplayRole>();
    QVariant v(QString(roleTxt.valueToKey((int)role)));
    s.setValue(DisplayRole, v);
}

bool Settings::scoreSpeech() const
{
    auto v = s.value(ScoreSpeech);
    return v.toBool();
}

void Settings::scoreSpeech(bool on)
{
    QVariant v(on);
    s.setValue(ScoreSpeech, v);
}

scoreboard::Protocol::DisplayFeature Settings::features() const
{
    auto ftTxt = QMetaEnum::fromType<scoreboard::Protocol::DisplayFeature>();
    auto v = s.value(DisplayFeatures);

    v.toString();
    bool ok=false;
    auto rv = (scoreboard::Protocol::DisplayFeature)ftTxt.keysToValue(v.toString().toStdString().c_str(), &ok);

    if(not ok)
        return scoreboard::Protocol::DisplayFeature::non;

    return rv;
}

void Settings::features(scoreboard::Protocol::DisplayFeature features)
{
    auto ftTxt = QMetaEnum::fromType<scoreboard::Protocol::DisplayFeature>();
    QVariant v(QString(ftTxt.valueToKey((int)features)));
    s.setValue(DisplayFeatures, v);
}

bool Settings::serverEnabled() const
{
    auto v = s.value(ServerEnabled);
    return v.toBool();
}

void Settings::serverEnabled(bool on)
{
    QVariant v(on);
    s.setValue(ServerEnabled, v);
}

scoreboard::Protocol::Event Settings::volUpKyeAction() const
{
    auto ftTxt = QMetaEnum::fromType<scoreboard::Protocol::Event>();
    auto v = s.value(VolumeUpAction);

    v.toString();
    bool ok=false;
    auto rv = (scoreboard::Protocol::Event)ftTxt.keysToValue(v.toString().toStdString().c_str(), &ok);

    if(not ok)
        return scoreboard::Protocol::Event::Non;

    return rv;
}

void Settings::volUpKyeAction(scoreboard::Protocol::Event e)
{
    auto ftTxt = QMetaEnum::fromType<scoreboard::Protocol::Event>();
    QVariant v(QString(ftTxt.valueToKey((int)e)));
    s.setValue(VolumeUpAction, v);
}

