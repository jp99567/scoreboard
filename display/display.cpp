#include "display.h"
#include <QVBoxLayout>
#include <QLabel>
#include <QDebug>
#include <QResizeEvent>

namespace scoreboard {

Display::Display(QWidget *parent)
    : QWidget(parent)
{
    main = new QLabel("8", this);
    QFont f = main->font();
    f.setPointSizeF(100);
    main->setFont(f);
    main->setAlignment(Qt::AlignTop|Qt::AlignLeft);
    auto palette = main->palette();
    palette.setColor(QPalette::Window, Qt::blue);
    main->setAutoFillBackground(true);
    main->setPalette(palette);

    aux = new QLabel("OO", this);
    aux->setAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
    palette = aux->palette();
    palette.setColor(QPalette::Window, Qt::yellow);
    aux->setAutoFillBackground(true);
    aux->setPalette(palette);
}

void Display::resizeEvent(QResizeEvent *event)
{
    qDebug() << width() << height();

    auto f = main->font();
    QFontMetrics fm(f);
    float ascentTotal = fm.ascent();
    float heightX = ascentTotal/1.24;
    float hwRatio = 1.1*ascentTotal / fm.horizontalAdvance('X');
    float hwRatioX = 1.1*heightX / fm.horizontalAdvance('X');
    auto newMainH = width() * hwRatio;
    auto newMainShowedH = width() * hwRatioX;
    auto newW = width();

    qDebug() << "heightX" << heightX << newMainH << newMainShowedH << heightX << ascentTotal;
    main->move(0,newMainShowedH-newMainH);
    main->resize(newW, newMainH);
    aux->move(0, newMainShowedH);
    auto newAuxH = height() - newMainShowedH;
    aux->resize(newW, newAuxH);

    float factor = (float)newMainH / fm.ascent();
    float factorAux = newAuxH / fm.height();
    qDebug() << main->width() << main->height()
         << "font" << fm.horizontalAdvance('X') << fm.ascent() << f.pointSizeF() << factor;

    float auxPointSize = f.pointSizeF()*factorAux;
    f.setPointSizeF(f.pointSizeF()*factor*0.95);
    main->setFont(f);
    f.setPointSizeF(auxPointSize);
    fm = QFontMetrics(f);
    float auxRatio = (float)fm.horizontalAdvance("OO") / newW;
    if(auxRatio > 1)
    {
        auxPointSize /= auxRatio;
        f.setPointSizeF(auxPointSize);
    }
    aux->setFont(f);
    QWidget::resizeEvent(event);
}

Display::~Display()
{
}

template<QPalette::ColorRole role>
static void setColorToWidget(QLabel *w, QString s)
{
    QColor color(s);
    auto palette = w->palette();
    palette.setColor(role, color);
    w->setPalette(palette);
}

void Display::setMainBgColor(QString s)
{
    setColorToWidget<QPalette::Window>(main, std::move(s));
}

void Display::setAuxBgColor(QString s)
{
    setColorToWidget<QPalette::Window>(aux, std::move(s));
}


void Display::setMainTxtColor(QString s)
{
    setColorToWidget<QPalette::WindowText>(main, std::move(s));
}

void Display::setAuxTxtColor(QString s)
{
    setColorToWidget<QPalette::WindowText>(aux, std::move(s));
}

void Display::setMainChar(QChar c)
{
    main->setText(c);
}

void Display::setAuxText(QString s)
{
    aux->setText(s);
}

}
