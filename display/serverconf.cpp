#include "serverconf.h"

#include <QVBoxLayout>
#include <QFormLayout>
#include <QLabel>
#include <QCheckBox>
#include <QTextEdit>
#include <QTime>
#include "scoreboard.h"
#include "settings.h"

ServerConf::ServerConf(Settings& settings)
    :settings(settings)
{
    auto enable = new QCheckBox;
    enable->setChecked(settings.serverEnabled());
    auto vlayout = new QVBoxLayout;
    auto flayout = new QFormLayout;
    flayout->addRow("Enable", enable);
    vlayout->addLayout(flayout);
    logW = new QTextEdit;
    logW->setReadOnly(true);
    vlayout->addWidget(logW);
    setLayout(vlayout);

    connect(enable, &QCheckBox::clicked, [this](bool checked){
        this->settings.serverEnabled(checked);
        enableServer(checked);
    });

    enableServer(enable->isChecked());
}

void ServerConf::enableServer(bool on)
{
    if(on)
    {
        if(server)
            return;

        log.clear();
        server = std::make_unique<Scoreboard>();
        appendMsg(QString("listening on %1").arg(server->interfaces()));
        connect(server.get(), &Scoreboard::infoMsg, this, &ServerConf::appendMsg);
    }
    else{
        if(server)
        {
            disconnect(server.get(), &Scoreboard::infoMsg, this, &ServerConf::appendMsg);
        }
        server = nullptr;
    }
}

void ServerConf::appendMsg(QString msg)
{
    while(log.count() > 20){
        log.pop_front();
    }

    log.append(QString("[%1] %2").arg(QTime::currentTime().toString()).arg(msg));
    QString text;
    foreach(QString i, log)
    {
        text.append(i + '\n');
    }

    logW->setText(text);
}
