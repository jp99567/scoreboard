#pragma once

#include "protocol.h"
#include <QSettings>

class Settings
{
    QSettings s;

public:
    explicit Settings();
    void sync()
    {
        s.sync();
    }

    bool connectionEnabled() const;
    void connectionEnabled(bool);
    QString host() const;
    void host(QString);
    scoreboard::Protocol::DisplayRole displayRole() const;
    void displayRole(scoreboard::Protocol::DisplayRole);
    bool scoreSpeech() const;
    void scoreSpeech(bool);
    scoreboard::Protocol::DisplayFeature features() const;
    void features(scoreboard::Protocol::DisplayFeature);
    bool serverEnabled() const;
    void serverEnabled(bool);
    scoreboard::Protocol::Event volUpKyeAction() const;
    void volUpKyeAction(scoreboard::Protocol::Event);
};
