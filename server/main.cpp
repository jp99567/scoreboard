#include <QCoreApplication>
#include <QTime>

#include "scoreboard.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Scoreboard sb;
    QObject::connect(&sb, &Scoreboard::infoMsg,[](QString msg){
        qDebug() << QTime::currentTime().toString() << msg;
    });
    return a.exec();
}
