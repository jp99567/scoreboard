#pragma once

#include <string>
#include <QObject>
#include <QtCore/QtGlobal>

#if defined(VOLSCORE_LIBRARY)
#  define VOLSCORE_LIBRARY_EXPORT Q_DECL_EXPORT
#else
#  define VOLSCORE_LIBRARY_EXPORT Q_DECL_IMPORT
#endif

class VOLSCORE_LIBRARY_EXPORT LineParser : public QObject
{
    Q_OBJECT
    std::string in;
public:
    LineParser();
    void clear();
    void process(std::string data);

signals:
    void newLine(std::string line);
};
