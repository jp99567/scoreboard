#include "LineParser.h"

#include <sstream>

LineParser::LineParser()
{

}

void LineParser::clear()
{
    in.clear();
}

void LineParser::process(std::string data)
{
    in.append(std::move(data));
    std::istringstream ts;
    ts.str(in);
    std::string line;
    while(std::getline(ts, line))
    {
        if(ts.eof())
        {
            in = std::move(line);
            return;
        }

        if(!line.empty())
        {
            emit newLine(std::move(line));
        }
    }
    in.clear();
}
