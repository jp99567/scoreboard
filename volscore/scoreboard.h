#pragma once

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <list>
#include <memory>

#include "protocol.h"
#include "LineParser.h"
#include <QtCore/QtGlobal>

#if defined(VOLSCORE_LIBRARY)
#  define VOLSCORE_LIBRARY_EXPORT Q_DECL_EXPORT
#else
#  define VOLSCORE_LIBRARY_EXPORT Q_DECL_IMPORT
#endif

namespace scoreboard
{

struct VOLSCORE_LIBRARY_EXPORT Score
{
    int b = 0;
    int a = 0;
    int setA = 0;
    int setB = 0;
    Protocol::Servis servis = Protocol::Servis::non;

    bool teamAonLeft() const;
    void swapSide();
    bool operator==(const Score& other) const
    {
        return other.a == a
            && other.b == b
            && other.setA == setA
            && other.setB == setB
            && other.servis == servis;
    }

    bool operator!=(const Score& other) const
    {
        return !(*this == other);
    }
};
}

class Client : public QObject
{
    Q_OBJECT

    QTcpSocket *socket;
    LineParser in;

public:
    explicit Client(QTcpSocket* socket);
    Client(const Client&) = delete;
    Client(Client&&) = delete;
    Client& operator=(const Client&) = delete;
    Client& operator=(Client&&) = delete;
    ~Client();

    void sendLine(QString line);
    QString getId()const
    {
        return id;
    }

signals:
    void disconnected(Client* thisConnection);
    void newLine(Client* thisConnection, std::string line);

private slots:
    void onReadyRead();
    void onDisconnect();

private:
    QString id;
    static int counter;
};

class VOLSCORE_LIBRARY_EXPORT Scoreboard : public QObject
{
    Q_OBJECT

    using ClientSptr = std::shared_ptr<Client>;
    QTcpServer listener;
    std::list<ClientSptr> clients;
    std::list<std::weak_ptr<Client>> audio;
    std::map<scoreboard::Protocol::DisplayRole,std::weak_ptr<Client>> display;

public:
    Scoreboard();
    static QString interfaces();
    static QString serializeScore(const scoreboard::Score& score);
    static QString scoreToStr(const scoreboard::Score& score);
    static void deserializeScore(std::istream& in, scoreboard::Score& out);
    static bool isSetFinished(int a, int b);
    static bool isMatchball(int a, int b);

signals:
    void infoMsg(QString);

private slots:
    void removeClient(Client* client);
    void processLine(Client* client, std::string line);

private:
    void updateRole(Client* client, scoreboard::Protocol::DisplayRole role);
    void checkRole(scoreboard::Protocol::DisplayRole role);
    void updateAudio(Client* client, bool add);
    void processEvent(scoreboard::Protocol::Event);
    void checkServisInit(scoreboard::Protocol::Servis);
    void displayFault(int code);
    void displayFault(scoreboard::Protocol::DisplayRole role, int code);
    void displayFault(Client& client, int code);
    ClientSptr getClientSptr(const Client* client);
    ClientSptr getDisplayRoleCLient(scoreboard::Protocol::DisplayRole role);
    void displayScore();
    void sendScore();
    void playSound(QString sound);

    void setMainDisplayText(Client& client, QChar c);
    void setMainDisplayTextColor(Client& client, QColor color);
    void setMainDisplayBgColor(Client& client, QColor color);
    void setAuxDisplayText(Client& client, QString text);
    void setAuxDisplayTextColor(Client& client, QColor color);
    void setAuxDisplayBgColor(Client& client, QColor color);

    scoreboard::Score score;
    bool firstServisLeft = false;
    std::list<scoreboard::Score> history;
};
