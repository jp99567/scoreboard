#pragma once

#include <QObject>

class FlicStupidHttpd : public QObject
{
    Q_OBJECT
public:
    explicit FlicStupidHttpd(QObject *parent = nullptr);

signals:
    void plusA();
    void plusB();
    void batteryLow(int level);
};
