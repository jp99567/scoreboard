#pragma once

#include <cinttypes>
#include <QObject>

#include <QtCore/QtGlobal>

#if defined(VOLSCORE_LIBRARY)
#  define VOLSCORE_LIBRARY_EXPORT Q_DECL_EXPORT
#else
#  define VOLSCORE_LIBRARY_EXPORT Q_DECL_IMPORT
#endif

namespace scoreboard
{

constexpr uint16_t port = 9233;
constexpr auto maxSetPoints = 25;

class VOLSCORE_LIBRARY_EXPORT Protocol : public QObject
{
    Q_OBJECT
public:
    enum class DisplayRole
    {
        non,
        L1,
        L2,
        R1,
        R2
    };
    Q_ENUM(DisplayRole)

    enum class Event
    {
        PlusA,
        PlusB,
        Non
    };
    Q_ENUM(Event)

    enum class Servis
    {
        non,
        A,
        B
    };
    Q_ENUM(Servis)

    enum class DisplayFeature
    {
        non = 0,
        PlaySound = 0x01,
        Feature1 = 0x02
    };
    Q_FLAG(DisplayFeature)
    Q_DECLARE_FLAGS(DisplayFeatures, DisplayFeature)

    enum class ServerRequest
    {
        InfoDisplayRole,
        InfoFeatures,
        AcceptEvent,
        SetScore, // ScoreA ScoreB SetA SetB Servis
        ScoreBack
    };
    Q_ENUM(ServerRequest)

    enum class ServerIndication
    {
        InfoScore, // ScoreA ScoreB SetA SetB Servis
        SetMainDisplayText,
        SetMainDisplayTextColor,
        SetMainDisplayBgColor,
        SetAuxDisplayText,
        SetAuxDisplayTextColor,
        SetAuxDisplayBgColor,
        PlaySound
    };
    Q_ENUM(ServerIndication)
};

Q_DECLARE_OPERATORS_FOR_FLAGS(Protocol::DisplayFeatures)

}
