#include "scoreboard.h"

#include <QDebug>
#include <QMetaEnum>
#include <QNetworkInterface>
#include <QColor>
#include <algorithm>
#include <sstream>
#include <cstdlib>

namespace  {
struct ScopedExit
{
    std::function<void()> f;

    ScopedExit(std::function<void()> f) : f(f){}

    ~ScopedExit()
    {
        f();
    }
};
}

Scoreboard::Scoreboard()
{
    connect(&listener, &QTcpServer::newConnection, this, [this]{
        do{
            auto tcpcon = listener.nextPendingConnection();
            if(!tcpcon)
                break;
            clients.emplace_back(std::make_shared<Client>(tcpcon));
            connect(clients.back().get(), &Client::disconnected, this, &Scoreboard::removeClient);
            connect(clients.back().get(), &Client::newLine, this, &Scoreboard::processLine);

            auto addr = tcpcon->peerAddress().toString();
            auto id = clients.back()->getId();
            auto msg = QString("connected %1(%2)").arg(id).arg(addr);
            emit infoMsg(msg);
        }
        while(listener.hasPendingConnections());
    });

    if( ! listener.listen(QHostAddress::Any, scoreboard::port))
    {
        qDebug() << listener.errorString();
    }

    qDebug() << "listening"
             << QNetworkInterface::allAddresses();
}

QString Scoreboard::interfaces()
{
    QString rv;
    QString p = "";
    for(auto &i : QNetworkInterface::allAddresses())
    {
        if(!i.isLoopback()){
            rv.append( p + i.toString() );
            p = " ";
        }
    }
    return rv;
}

QString Scoreboard::serializeScore(const scoreboard::Score &score)
{
    auto servisTxt = QMetaEnum::fromType<scoreboard::Protocol::Servis>();
    return QString("%1 %2 %3 %4 %5")
            .arg(score.a)
            .arg(score.b)
            .arg(score.setA)
            .arg(score.setB)
            .arg(servisTxt.valueToKey((int)score.servis));
}

QString Scoreboard::scoreToStr(const scoreboard::Score &score)
{
    auto servisTxt = QMetaEnum::fromType<scoreboard::Protocol::Servis>();
    return QString("%1:%2(%3:%4)%5")
            .arg(score.a)
            .arg(score.b)
            .arg(score.setA)
            .arg(score.setB)
            .arg(servisTxt.valueToKey((int)score.servis));
}

void Scoreboard::deserializeScore(std::istream &in, scoreboard::Score &out)
{
    auto servisTxt = QMetaEnum::fromType<scoreboard::Protocol::Servis>();
    in >> out.a;
    in >> out.b;
    in >> out.setA;
    in >> out.setB;
    std::string servis;
    in >> servis;
    out.servis = (scoreboard::Protocol::Servis)servisTxt.keyToValue(servis.c_str());
}

void Scoreboard::removeClient(Client *client)
{
    QString id("non");
    if(client)
        id = client->getId();

    auto toRemove = std::find_if(std::begin(clients),
                 std::end(clients), [client](auto p){
       return p.get() == client;
    });

    if(toRemove == std::end(clients))
    {
        qDebug() << "no client to remove" << client;
        return;
    }
    disconnect(toRemove->get(), &Client::newLine, this, &Scoreboard::processLine);
    clients.erase(toRemove);

    emit infoMsg(QString("disconnected %1").arg(id));
}

void Scoreboard::processLine(Client *client, std::string line)
{
    //qDebug() << "processLine" << client << QString::fromStdString(line);
    std::istringstream ts;
    ts.str(line);
    std::string argTxt;
    ts >> argTxt;

    using scoreboard::Protocol;
    bool ok = false;
    auto req = (Protocol::ServerRequest)QMetaEnum::fromType<Protocol::ServerRequest>().keyToValue(argTxt.c_str(), &ok);

    if(not ok)
    {
        removeClient(client);
        return;
    }

    switch(req)
    {
    case Protocol::ServerRequest::InfoDisplayRole:
      {
        ts >> argTxt;
        bool ok = false;
        auto role = (Protocol::DisplayRole)QMetaEnum::fromType<Protocol::DisplayRole>()
                .keyToValue(argTxt.c_str(), &ok);
        if(ok)
        {
            updateRole(client, role);
            checkRole(role);
        }
      }
        break;
    case Protocol::ServerRequest::AcceptEvent:
      {
        ts >> argTxt;
        bool ok = false;
        auto e = (Protocol::Event)QMetaEnum::fromType<Protocol::Event>()
                .keyToValue(argTxt.c_str(), &ok);
        if(ok)
            processEvent(e);
      }
        break;
    case Protocol::ServerRequest::InfoFeatures:
      {
        ts >> argTxt;
        bool ok = false;
        auto ft = (Protocol::DisplayFeature)QMetaEnum::fromType<Protocol::DisplayFeature>()
                .keyToValue(argTxt.c_str(), &ok);
        if(ok)
        {
            QFlags<Protocol::DisplayFeature> fts(ft);
            updateAudio(client, fts.testFlag(Protocol::DisplayFeature::PlaySound));
        }
      }
        break;
    case Protocol::ServerRequest::SetScore:
      {
        qDebug() << QString::fromStdString(ts.str());
        playSound("ding.mp3");
        scoreboard::Score newScore;
        Scoreboard::deserializeScore(ts,newScore);
        checkServisInit(newScore.servis);
        score = newScore;
        history.clear();
        emit infoMsg(QString("corrected %1").arg(scoreToStr(score)));
        sendScore();
        displayScore();
      }
        break;
    case Protocol::ServerRequest::ScoreBack:
        if(not history.empty())
        {
            playSound("ding.mp3");
            {
                auto& newScore = history.back();
                checkServisInit(newScore.servis);
                score = newScore;
                history.pop_back();
            }
            emit infoMsg(QString("score back %1").arg(scoreToStr(score)));
            sendScore();
            displayScore();
        }
        else{
            qDebug() << "history empty";
        }
        break;
    default:
        qDebug() << "unknown" << req;
        break;
    }
}

constexpr scoreboard::Protocol::DisplayRole validRoles[] = {
   scoreboard::Protocol::DisplayRole::L1,
   scoreboard::Protocol::DisplayRole::L2,
   scoreboard::Protocol::DisplayRole::R1,
   scoreboard::Protocol::DisplayRole::R2
};

void Scoreboard::updateRole(Client *client, scoreboard::Protocol::DisplayRole role)
{
    qDebug() << client << role;

    auto ct = std::find_if(std::begin(display), std::end(display), [client](auto c){
        auto sptr = c.second.lock();
        return sptr && sptr.get() == client;
    });

    if(ct != std::end(display))
    {
        if(ct->first == role)
        {
            return;
        }
        else{
            display.erase(ct->first);
        }
    }

    auto ad = display.find(role);
    if(ad != std::end(display))
    {
        displayFault(role, 1);
    }

    if(role != scoreboard::Protocol::DisplayRole::non)
    {
        auto sptr = getClientSptr(client);
        if(sptr)
            display[role] = sptr;
    }
}

void Scoreboard::checkRole(scoreboard::Protocol::DisplayRole role)
{
    auto L1 = getDisplayRoleCLient(scoreboard::Protocol::DisplayRole::L1);
    auto L2 = getDisplayRoleCLient(scoreboard::Protocol::DisplayRole::L2);
    auto R1 = getDisplayRoleCLient(scoreboard::Protocol::DisplayRole::R1);
    auto R2 = getDisplayRoleCLient(scoreboard::Protocol::DisplayRole::R2);

    QString bg = "yellow";
    if(L1 && L2 && R1 && R2)
    {
        bg = "green";
        if(role == scoreboard::Protocol::DisplayRole::non)
            return;
    }

    for( auto sptr : {L1, L2, R1, R2})
    {
        if(sptr)
        {
            setAuxDisplayBgColor(*sptr,bg);
            setMainDisplayBgColor(*sptr,bg);
            setAuxDisplayTextColor(*sptr, "brown");
            setMainDisplayTextColor(*sptr, "brown");
        }
    }

    for(auto& i : {
                std::make_tuple(L1, 'L', "1"),
                std::make_tuple(L2, 'L', "2"),
                std::make_tuple(R1, 'R', "1"),
                std::make_tuple(R2, 'R', "2")
                })
    {
        if(std::get<0>(i))
        {
            setAuxDisplayText(*std::get<0>(i), std::get<2>(i));
            setMainDisplayText(*std::get<0>(i), std::get<1>(i));
        }
    }
}

void Scoreboard::updateAudio(Client *client, bool add)
{
    if(add)
    {
        if(!client)
            return;
        for(auto wp : audio)
        {
            if(wp.lock().get() == client)
                return;
        }
        audio.emplace_back(getClientSptr(client));
    }
    else {
        auto toRemove = std::remove_if(std::begin(audio), std::end(audio), [client](auto wptr){
            return wptr.lock().get() == client;
        });

        if(toRemove != std::end(audio))
            audio.erase(toRemove);
    }
}

bool Scoreboard::isSetFinished(int a, int b)
{
    return std::max(a, b) >= scoreboard::maxSetPoints
            && std::abs(a-b) > 1;
}

bool Scoreboard::isMatchball(int a, int b)
{
    return std::max(a,b) >= scoreboard::maxSetPoints - 1
        && std::abs(a-b) > 0
        && !isSetFinished(a,b);
}

void Scoreboard::processEvent(scoreboard::Protocol::Event e)
{
    ScopedExit updateHistory([origScore=score,this]{
        if(score != origScore)
            history.push_back(origScore);
    });

    checkServisInit( e == scoreboard::Protocol::Event::PlusA
                     ? score.servis = scoreboard::Protocol::Servis::A
                     : score.servis = scoreboard::Protocol::Servis::B);

    if(isSetFinished(score.a,score.b))
    {
        if(score.a > score.b)
            ++score.setA;
        else
            ++score.setB;

        score.a = score.b = 0;
        if(score.teamAonLeft() ^ firstServisLeft)
            score.servis = scoreboard::Protocol::Servis::A;
        else
            score.servis = scoreboard::Protocol::Servis::B;

        sendScore();
        displayScore();
        emit infoMsg(QString("next set: %1").arg(scoreToStr(score)));
        return;
    }

    using scoreboard::Protocol;

    switch (e) {
    case scoreboard::Protocol::Event::PlusA:
        if(score.servis != Protocol::Servis::non)score.a++;
        score.servis = scoreboard::Protocol::Servis::A;
        emit infoMsg(QString("plusA %1").arg(scoreToStr(score)));
        break;
    case scoreboard::Protocol::Event::PlusB:
        if(score.servis != Protocol::Servis::non)score.b++;
        score.servis = scoreboard::Protocol::Servis::B;
        emit infoMsg(QString("plusB %1").arg(scoreToStr(score)));
        break;
    default:
        break;
    }
    if(isSetFinished(score.a,score.b))
    {
        playSound("choral.mp3");
    }
    else {
        playSound( e == scoreboard::Protocol::Event::PlusA
                   ? "dingA.mp3"
                   : "dingB.mp3" );
    }

    sendScore();
    displayScore();
}

void Scoreboard::checkServisInit(scoreboard::Protocol::Servis newServis)
{
    if(score.servis == scoreboard::Protocol::Servis::non)
    {
        firstServisLeft = newServis == scoreboard::Protocol::Servis::A;
    }
}

void Scoreboard::displayFault(int code)
{
    for(auto role : validRoles)
        displayFault(role, code);
}

Scoreboard::ClientSptr Scoreboard::getDisplayRoleCLient(scoreboard::Protocol::DisplayRole role)
{
    auto it = display.find(role);

    if( it == std::end(display))
        return nullptr;

    return it->second.lock();
}

void Scoreboard::displayFault(scoreboard::Protocol::DisplayRole role, int code)
{
    auto sptr = getDisplayRoleCLient(role);

    if(sptr)
    {
        displayFault(*sptr, code);
    }
}

void Scoreboard::displayFault(Client& client, int code)
{
    setMainDisplayBgColor(client, "red");
    setAuxDisplayBgColor(client, "red");
    setMainDisplayTextColor(client, "cyan");
    setAuxDisplayTextColor(client, "cyan");
    setMainDisplayText(client, 'F');
    setAuxDisplayText(client, QString::number(code));
}

Scoreboard::ClientSptr Scoreboard::getClientSptr(const Client *client)
{
    auto it = std::find_if(std::begin(clients), std::end(clients), [client](auto c){
        return c.get() == client;
    });

    if(it == std::end(clients))
        return nullptr;

    return *it;
}

void Scoreboard::displayScore()
{
    auto L1 = getDisplayRoleCLient(scoreboard::Protocol::DisplayRole::L1);
    auto L2 = getDisplayRoleCLient(scoreboard::Protocol::DisplayRole::L2);
    auto R1 = getDisplayRoleCLient(scoreboard::Protocol::DisplayRole::R1);
    auto R2 = getDisplayRoleCLient(scoreboard::Protocol::DisplayRole::R2);

    if( not (L1 && L2 && R1 && R2))
    {
        qDebug() << "display digits not complet";
        displayFault(2);
        return;
    }

    auto score = this->score;

    if( not score.teamAonLeft())
        score.swapSide();

    for(auto disp : {L1, L2, R1, R2})
    {
        setMainDisplayBgColor(*disp, "black");
        setAuxDisplayBgColor(*disp, "black");
        setAuxDisplayTextColor(*disp, "cyan");
    }

    QString textColor[2] = {"white", "white"};
    if(score.servis == scoreboard::Protocol::Servis::A)
    {
        textColor[0] = "green";
    }
    else if(score.servis == scoreboard::Protocol::Servis::B)
    {
        textColor[1] = "green";
    }

    for(auto disp : {L1, L2})
    {
        setMainDisplayTextColor(*disp, textColor[0]);
    }

    for(auto disp : {R1, R2})
    {
        setMainDisplayTextColor(*disp, textColor[1]);
    }

    int numL1 = score.a / 10;
    int numR1 = score.b / 10;
    int numL2 = score.a % 10;
    int numR2 = score.b % 10;

    QChar cL1 = numL1 == 0 ? ' ' : QString::number(numL1).at(0);
    QChar cR1 = numR1 == 0 ? ' ' : QString::number(numR1).at(0);
    QChar cL2 = QString::number(numL2).at(0);
    QChar cR2 = QString::number(numR2).at(0);

    QChar ca[] = {cL1, cL2, cR1, cR2};

    int i=0;
    for(auto disp : {L1, L2, R1, R2})
    {
        setMainDisplayText(*disp, ca[i++]);
    }

    constexpr int dispSetNr = 2;
    constexpr int pointsPerDisp = 2;
    if(std::max(score.setA, score.setB) > dispSetNr*pointsPerDisp)
    {
        setAuxDisplayText(*L2, QString::number(score.setA));
        setAuxDisplayText(*R2, QString::number(score.setB));
        setAuxDisplayText(*L1, "");
        setAuxDisplayText(*R1, "");
    }
    else
    {
        using DispSetArray = QString[dispSetNr];
        auto setPoints = [](int setNr, DispSetArray& disp){
        int setIdx = 0;
            while(setIdx++ < setNr){
                const QString bod = "◆";
                disp[(setIdx-1)/pointsPerDisp].append(bod);
            }
        };

        DispSetArray set[2];
        setPoints(score.setA, set[0]);
        setPoints(score.setB, set[1]);
        setAuxDisplayText(*L1, set[0][0]);
        setAuxDisplayText(*L2, set[0][1]);
        setAuxDisplayText(*R1, set[1][0]);
        setAuxDisplayText(*R2, set[1][1]);
    }
}

void Scoreboard::sendScore()
{
    auto indTxt = QMetaEnum::fromType<scoreboard::Protocol::ServerIndication>();
    QString ind = QString(indTxt.valueToKey((int)scoreboard::Protocol::ServerIndication::InfoScore))
            + ' ' + serializeScore(score);
    for(auto client : clients)
    {
        client->sendLine(ind);
    }
}

void Scoreboard::playSound(QString sound)
{
    auto indTxt = QMetaEnum::fromType<scoreboard::Protocol::ServerIndication>();
    QString ind = QString(indTxt.valueToKey((int)scoreboard::Protocol::ServerIndication::PlaySound))
            + ' ' + sound;

    for(auto client : audio)
    {
        auto sptr = client.lock();
        if(sptr)
            sptr->sendLine(ind);
    }
}

void Scoreboard::setMainDisplayText(Client& client, QChar c)
{
    auto indTxt = QMetaEnum::fromType<scoreboard::Protocol::ServerIndication>();
    QString ind = QString(indTxt.valueToKey((int)scoreboard::Protocol::ServerIndication::SetMainDisplayText))
            + ' ' + c;
    client.sendLine(std::move(ind));
}

template<scoreboard::Protocol::ServerIndication IND>
void setDisplayColorIndication(Client& client, const QColor& color)
{
    auto indTxt = QMetaEnum::fromType<scoreboard::Protocol::ServerIndication>();
    QString ind = QString(indTxt.valueToKey((int)IND))
            + ' ' + color.name();
    client.sendLine(std::move(ind));
}

void Scoreboard::setMainDisplayTextColor(Client& client, QColor color)
{
    setDisplayColorIndication<scoreboard::Protocol::ServerIndication::SetMainDisplayTextColor>(client, color);
}

void Scoreboard::setMainDisplayBgColor(Client& client, QColor color)
{
    setDisplayColorIndication<scoreboard::Protocol::ServerIndication::SetMainDisplayBgColor>(client, color);
}

void Scoreboard::setAuxDisplayText(Client &client, QString text)
{
    auto indTxt = QMetaEnum::fromType<scoreboard::Protocol::ServerIndication>();
    QString ind = QString(indTxt.valueToKey((int)scoreboard::Protocol::ServerIndication::SetAuxDisplayText))
            + ' ' + text;
    client.sendLine(std::move(ind));
}

void Scoreboard::setAuxDisplayTextColor(Client& client, QColor color)
{
    setDisplayColorIndication<scoreboard::Protocol::ServerIndication::SetAuxDisplayTextColor>(client, color);
}

void Scoreboard::setAuxDisplayBgColor(Client& client, QColor color)
{
    setDisplayColorIndication<scoreboard::Protocol::ServerIndication::SetAuxDisplayBgColor>(client, color);
}

int Client::counter = 0;

Client::Client(QTcpSocket *socket)
    :socket(socket)
    ,id(QString("C%1").arg(++counter))
{
    connect(socket, &QTcpSocket::readyRead, this, &Client::onReadyRead);
    connect(socket, &QTcpSocket::disconnected, this, &Client::onDisconnect);
    connect(&in, &LineParser::newLine, [this](std::string line){
       emit newLine(this, line);
    });
    qDebug() << "new socket from:" << socket->peerAddress();
}

Client::~Client()
{
    auto ok = disconnect(socket, &QTcpSocket::disconnected, this, &Client::onDisconnect);
    socket->deleteLater();
    qDebug() << "client destuct" << ok;
}

void Client::sendLine(QString line)
{
    //qDebug() << "sendLine" << line;
    line.append('\n');
    socket->write(line.toUtf8());
}

void Client::onReadyRead()
{
    auto data = socket->readAll();
    qDebug() << "onReadyRead" << data;
    in.process(data.toStdString());
}

void Client::onDisconnect()
{
    in.clear();
    emit disconnected(this);
}

bool scoreboard::Score::teamAonLeft() const
{
    return (setA + setB) % 2 == 0;
}

void scoreboard::Score::swapSide()
{
    std::swap(a,b);
    std::swap(setA,setB);

    if(servis == Protocol::Servis::A)
        servis = Protocol::Servis::B;
    else if(servis == Protocol::Servis::B)
        servis = Protocol::Servis::A;
}
