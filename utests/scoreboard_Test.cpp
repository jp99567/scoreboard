#include <QtTest/QtTest>

#include "../lib/scoreboard.h"
#include <sstream>

class TestScoreboard: public QObject
{
    Q_OBJECT
private slots:
    void serializeScore();
    void voleyball();
};

void TestScoreboard::serializeScore()
{
    scoreboard::Score score;
    QCOMPARE(Scoreboard::serializeScore(score), QString("0 0 0 0 non"));
    std::istringstream in("1 2 3 4 A");
    Scoreboard::deserializeScore(in, score);
    QCOMPARE(Scoreboard::serializeScore(score), QString("1 2 3 4 A"));
}

void TestScoreboard::voleyball()
{
    QCOMPARE(Scoreboard::isMatchball(0,0), false);
    QCOMPARE(Scoreboard::isMatchball(5,0), false);
    QCOMPARE(Scoreboard::isMatchball(11,11), false);
    QCOMPARE(Scoreboard::isMatchball(24,24), false);
    QCOMPARE(Scoreboard::isSetFinished(24,24), false);
    QCOMPARE(Scoreboard::isMatchball(25,25), false);
    QCOMPARE(Scoreboard::isMatchball(30,30), false);
    QCOMPARE(Scoreboard::isSetFinished(23,24), false);
    QCOMPARE(Scoreboard::isMatchball(23,24), true);
    QCOMPARE(Scoreboard::isMatchball(1,24), true);
    QCOMPARE(Scoreboard::isMatchball(30,31), true);
    QCOMPARE(Scoreboard::isSetFinished(30,31), false);
    QCOMPARE(Scoreboard::isMatchball(25,24), true);
    QCOMPARE(Scoreboard::isMatchball(25,27), false);
    QCOMPARE(Scoreboard::isSetFinished(25,27), true);
}

QTEST_MAIN(TestScoreboard)
#include "scoreboard_Test.moc"
